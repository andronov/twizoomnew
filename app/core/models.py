from django.db import models

# Create your models here.

class BaseModel(models.Model):
    is_active = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class Trends(BaseModel):
    name = models.CharField(blank=True,max_length=350)
    is_hastag = models.BooleanField(default=True)

    class Meta:
        ordering = ['-created']

    def is_top(self):
        tr = TopTweets.objects.get(trend=self).tweets.all().count()
        if tr and tr >= 100:
           return tr
        else:
           return False

class Tweet(BaseModel):
    trend = models.ForeignKey(Trends,null=True,related_name='tweet_trend')

    id_tweet = models.IntegerField(blank=True)
    retweet_count = models.IntegerField(default=0,blank=True,null=True)
    favorite_count = models.IntegerField(default=0,blank=True,null=True)
    common_count = models.IntegerField(default=0,blank=True,null=True)
    content = models.CharField(max_length=1000)
    user_screen_name = models.CharField(blank=True, max_length=250)
    user_name = models.CharField(blank=True, max_length=250)
    user_id = models.CharField(blank=True, max_length=250)
    url = models.CharField(blank=True, max_length=250)
    user_lang = models.CharField(blank=True, max_length=250)
    place = models.IntegerField(default=1,blank=True,null=True)

    notification = models.IntegerField(default=2)

    class Meta:
        ordering = ['-common_count']


class TopTweets(BaseModel):
    trend = models.ForeignKey(Trends,null=True,related_name='top_trend')
    tweets = models.ManyToManyField(Tweet, blank=True, related_name='top_tweets')

class RelatedTweets(BaseModel):
    trend = models.ForeignKey(Trends,null=True,related_name='related_trend')
    item = models.ForeignKey(Tweet,null=True,related_name='related_item')
    items = models.ManyToManyField(Tweet,blank=True,related_name="related_items")


