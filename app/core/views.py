import json
import random
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from rest_framework.generics import RetrieveAPIView, get_object_or_404, ListAPIView

from twitter import *


# Create your views here.
from core.models import Tweet, Trends, TopTweets, RelatedTweets
from ttp import ttp
from core.serializers import TopTweetsSerializer, TweetSerializer, TrendSerializer
from core.utils import groups_tweets, proccess_tweets


def round_places(place):
        return (place + 9) // 10 * 10

class TweetsDetail(RetrieveAPIView):
    model = Tweet
    queryset = Tweet.objects.all()
    serializer_class = TweetSerializer
    lookup_field = 'id_tweet'

    def retrieve(self, request, *args, **kwargs):
        response = super(TweetsDetail, self).retrieve(request, *args, **kwargs)
        round_place = round_places(self.get_object().place)
        round_placet = round_place - 5
        tweets = Tweet.objects.filter(place__range=[round_placet,round_place])

        response.data = {
            'object': response.data,
            'tweets': [TweetSerializer(item).data for item in tweets],
        }
        return response

class UpdatesDetail(RetrieveAPIView):
    model = Tweet
    queryset = Tweet.objects.all()
    serializer_class = TweetSerializer
    lookup_field = 'id_tweet'

    def retrieve(self, request, *args, **kwargs):
        response = super(UpdatesDetail, self).retrieve(request, *args, **kwargs)
        related = [t.id for t in RelatedTweets.objects.get(item=self.get_object()).items.all()]



        response.data = {
            'object': response.data,
            'tweets': [TweetSerializer(item).data for item in Tweet.objects.filter(id__in=related)],
        }
        return response

class TopTweetsWordsDetail(RetrieveAPIView):
    model = Trends
    queryset = Trends.objects.filter(is_hastag=False)
    serializer_class = TrendSerializer
    lookup_field = 'name'

    def retrieve(self, request, *args, **kwargs):
        response = super(TopTweetsWordsDetail, self).retrieve(request, *args, **kwargs)
        if 'count' in self.request.GET:
            print('yes')
            tweets = Tweet.objects.filter(trend=self.get_object())[:self.request.GET['count']]
        else:
            tweets = Tweet.objects.filter(trend=self.get_object())[:20]


        response.data = {
            'object': response.data,
            'tweets': [TweetSerializer(item).data for item in tweets],
        }
        return response

class TopTweetsDetail(RetrieveAPIView):
    model = Trends
    queryset = Trends.objects.filter(is_hastag=True)
    serializer_class = TrendSerializer
    lookup_field = 'name'

    def retrieve(self, request, *args, **kwargs):
        response = super(TopTweetsDetail, self).retrieve(request, *args, **kwargs)
        if 'count' in self.request.GET:
            print('yes')
            tweets = Tweet.objects.filter(trend=self.get_object())[:self.request.GET['count']]
        else:
            tweets = Tweet.objects.filter(trend=self.get_object())[:20]


        response.data = {
            'object': response.data,
            'tweets': [TweetSerializer(item).data for item in tweets],
        }
        return response

def test_trends(request):



    some_data_to_dump = {
    'some_var_1': 'foo',
    'some_var_2': 'bar',
    }
    data = json.dumps(some_data_to_dump)
    res = HttpResponse(data, content_type='application/json; charset=utf-8')
    res["Access-Control-Allow-Origin"] = "*"
    res["Access-Control-Allow-Headers"] = "*"
    return res



def info(request):
    trend = Trends.objects.get(id=1)
    twitter = Twitter(
    auth=OAuth(settings.TOKEN, settings.TOKEN_KEY, settings.CON_SECRET, settings.CON_SECRET_KEY))

    items = twitter.search.tweets(q="#nhl",result_type='recent',count='100')
    proccess_tweets(trend,items)

    items = twitter.search.tweets(q="#nhl",result_type='popular',count='20')
    proccess_tweets(trend,items)

    tweets = Tweet.objects.filter(trend=trend)
    if tweets.count() >= 100:
        obj = TopTweets.objects.create(trend=trend)
        i = 1
        for tweet in tweets:
            tweet.place = i
            tweet.save()
            obj.tweets.add(tweet)
            obj.save()
            i += 1
        obj.is_active = True
        obj.save()

        groups_tweets([s.id for s in tweets])
    else:
        print("Error")

