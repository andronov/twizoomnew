from rest_framework import serializers
from core.models import Trends, Tweet, TopTweets, RelatedTweets


class TrendSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)

    class Meta:
        model = Trends
        fields = ('pk', 'name', 'is_hastag')



class TweetSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)
    id_tweet = serializers.CharField(read_only=True)
    place = serializers.IntegerField(read_only=True)
    trend = TrendSerializer(many=False, read_only=True)
    content = serializers.CharField(required=False, allow_blank=True, max_length=1000)
    round_place  = serializers.SerializerMethodField('round_places')

    notification  = serializers.SerializerMethodField('noti')

    def round_places(self, place):
        return (place.place + 9) // 10 * 10

    def noti(self, obj):
        return RelatedTweets.objects.get(item=obj).items.count()

    class Meta:
        model = Tweet
        fields = ('pk','id_tweet','trend', 'content','place','round_place','notification', 'retweet_count', 'favorite_count',
                  'common_count', 'user_screen_name','user_name')


class TopTweetsSerializer(serializers.ModelSerializer):
    trend = TrendSerializer(many=False, read_only=True)
    tweets = TweetSerializer(many=True, read_only=True)

    class Meta:
        model = TopTweets
        fields = ('pk', 'trend', 'tweets')

class RelatedTweetsSerializer(serializers.ModelSerializer):
    trend = TrendSerializer(many=False, read_only=True)
    items = TweetSerializer(many=True, read_only=True)

    class Meta:
        model = RelatedTweets
        fields = ('pk', 'trend', 'items')
