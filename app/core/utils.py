from difflib import SequenceMatcher
from operator import itemgetter
from django.db.models import F, Sum
from ttp import ttp
from core.models import Tweet, RelatedTweets
import random

def text_sequence(text1, text2, isjunk=None):
    return SequenceMatcher(isjunk, text1, text2).ratio()


def groups_tweets(id):
    tweets = Tweet.objects.filter(id__in=id).annotate(count_sum=Sum(F('retweet_count')+F('favorite_count')))\
        .order_by('-count_sum')

    for base in tweets:
        result = []
        for extra in tweets.exclude(id_tweet=base.id_tweet):
            similiar = text_sequence(base.content,extra.content)
            result.append([similiar, extra.id])

        related = RelatedTweets.objects.get(item=base)
        for s in sorted(result, key=itemgetter(0),reverse=True)[:base.notification]:
            res = Tweet.objects.get(id=s[1])
            related.items.add(res)
            related.save()

def proccess_tweets(trend, items):
    notification = random.randint(1,12)
    for item in items['statuses']:
        #print(ttp.Parser().parse(item['text']).tags)
        if 'retweeted_status' not in item:
            tweet, created = Tweet.objects.get_or_create(id_tweet=item['id'],
                                                         content=ttp.Parser().parse(item['text']).html,
                defaults={'id_tweet':item['id'],
                         "trend": trend,
                         "retweet_count": item['retweet_count'],
                         "favorite_count": item['favorite_count'],
                         "common_count": item['retweet_count'] + item['favorite_count'],
                         "content": ttp.Parser().parse(item['text']).html,
                         "user_screen_name": item['user']['screen_name'],
                         "user_name": item['user']['name'],
                         "user_id": item['user']['id'],
                         "notification": notification,
                         "user_lang": item['user']['lang']})
            obj = RelatedTweets.objects.get_or_create(item=tweet,defaults={'trend': trend})