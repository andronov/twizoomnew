from django.contrib import admin

# Register your models here.
from core.models import *

class TweetAdmin(admin.ModelAdmin):
    list_display = ('id_tweet','retweet_count','favorite_count','place', 'content')

admin.site.register(Trends)
admin.site.register(Tweet, TweetAdmin)
admin.site.register(TopTweets)
admin.site.register(RelatedTweets)