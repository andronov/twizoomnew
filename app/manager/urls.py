from django.conf.urls import patterns, url, include
from django.core.urlresolvers import reverse_lazy
from manager.views import *

manager_urls = patterns(
    '',
    url(r'^$', ListTrendView.as_view(), name="manager_home"),
    url(r'^trend/edit/(?P<pk>[-\w]+)/$', TrendUpdateView.as_view(success_url=reverse_lazy('manager:manager_home')),
        name='manager_trend_list'),
    url(r'^trend/add/$', TrendCreateView.as_view(success_url=reverse_lazy('manager:manager_home')),
        name='manager_trend_add'),

)

api_manager_urls = patterns(
    '',
    url(r'^create/', createtop, name='create+api'),
)
