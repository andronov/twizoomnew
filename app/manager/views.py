import json
import random
from time import timezone
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.generic import ListView, UpdateView, CreateView
from twitter import Twitter, OAuth
from core.models import Trends, Tweet, RelatedTweets, TopTweets
from ttp import ttp
from core.utils import groups_tweets, proccess_tweets


class ListTrendView(ListView):
    model = Trends
    template_name = "manager/trend_list.html"

    def get_context_data(self, **kwargs):
        context = super(ListTrendView, self).get_context_data(**kwargs)
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('manager:manager_home')
        return super(ListTrendView, self).dispatch(*args, **kwargs)

class TrendUpdateView(UpdateView):
    model = Trends
    fields = '__all__'
    template_name = "manager/trend_update.html"

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('manager:manager_home')
        return super(TrendUpdateView, self).dispatch(*args, **kwargs)

class TrendCreateView(CreateView):
    model = Trends
    fields = '__all__'
    template_name = "manager/trend_add.html"

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('manager:manager_home')
        return super(TrendCreateView, self).dispatch(*args, **kwargs)

def createtop(request):
    success = False
    counter = 0

    try:
      trend = Trends.objects.get(id=request.GET['trend'])
      twitter = Twitter(
      auth=OAuth(settings.TOKEN, settings.TOKEN_KEY, settings.CON_SECRET, settings.CON_SECRET_KEY))

      if trend.is_hastag:
          items = twitter.search.tweets(q="#"+trend.name,result_type='recent',count='100')
          proccess_tweets(trend,items)
          items = twitter.search.tweets(q="#"+trend.name,result_type='popular',count='20')
          proccess_tweets(trend,items)
      else:
          items = twitter.search.tweets(q=trend.name,result_type='recent',count='100')
          proccess_tweets(trend,items)
          items = twitter.search.tweets(q=trend.name,result_type='popular',count='20')
          proccess_tweets(trend,items)


      tweets = Tweet.objects.filter(trend=trend)
      if tweets.count() >= 100:
        obj = TopTweets.objects.create(trend=trend)
        i = 1
        for tweet in tweets:
            tweet.place = i
            tweet.save()
            obj.tweets.add(tweet)
            obj.save()
            i += 1
        obj.is_active = True
        obj.save()

        groups_tweets([s.id for s in tweets])

        success = True
      else:
          success = False
          counter = tweets.count()
    except:
        success = False

    data_to_dump = {
        'success': success,
        'counter': counter
    }
    data = json.dumps(data_to_dump)
    res = HttpResponse(data, content_type='application/json; charset=utf-8')
    return res

