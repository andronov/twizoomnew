"""zamly URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url, patterns
from django.contrib import admin
from manager.urls import api_manager_urls, manager_urls
from core.views import *

api_packages_urls = patterns(
    '',
    url(r'^tweets/top/(?P<name>[^/]+)/', TopTweetsDetail.as_view()),
    url(r'^tweets/top/w/(?P<name>[^/]+)/', TopTweetsWordsDetail.as_view()),
    url(r'^tweets/updates/(?P<id_tweet>\d+)/', UpdatesDetail.as_view()),
    url(r'^tweets/(?P<id_tweet>\d+)/', TweetsDetail.as_view()),
)

api_urls = patterns(
    '',
    url(r'^api/v1/', include(api_packages_urls, namespace='api')),
    url(r'^api/v1/manager/', include(api_manager_urls, namespace='api_manager')),
)

urlpatterns = patterns(
    '',
    url(r'^info', info, name='info'),
    url(r'^manager/', include(manager_urls, namespace='manager')),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += api_urls

